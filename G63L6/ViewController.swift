//
//  ViewController.swift
//  G63L6
//
//  Created by Ivan Vasilevich on 6/17/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	enum JoystickButton: Int {
		case xMinus
		case xPlus
		case yPlus
		case yMinus
	}
	
	@IBOutlet weak var centerLabel: UILabel!
	var phone: Phone!
	var xPosition = 0
	var yPosition = 0
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		phone = Phone(screen: 5.5, memory: 16)
	}
	@IBAction func buttonPrezzed() {
		let textToDisplay = "pew pew \(arc4random()%100)"
		print(textToDisplay)
		centerLabel.text = textToDisplay
		
	}
	
	@IBAction func shootPressed() {
		phone.makePhoto()
	}
	@IBAction func descriptionButtonPressed() {
		centerLabel.text = phone.description
	}
	
	
	@IBAction func changePositionButtonPressed(_ sender: UIButton) {
//		if sender.tag == 1 {
//			xPosition += 1
//		}
//		else {
//			xPosition -= 1
//		}
//		sender.tag == 1 ? (xPosition += 1) : (xPosition -= 1)
//		xPosition += sender.tag == 1 ? 1 : -1
		
		if let joystickButton = JoystickButton(rawValue: sender.tag) {
			switch joystickButton {
			case .xMinus:
				xPosition -= 1
			case .xPlus:
				xPosition += 1
			case .yMinus:
				yPosition -= 1
			case .yPlus:
				yPosition += 1
			}
		}
		else {
			print("unknown tag \(sender.tag)")
		}
		
//		switch sender.tag {
//		case 1:
//			xPosition += 1
//		case 0:
//			xPosition -= 1
//		default:
//			print("unknown tag \(sender.tag)")
//		}
		
		print(xPosition)
		centerLabel.text = xPosition.description + " " +  yPosition.description
	}
	
	
	
}

